#include "pch.h"
#include <iostream>

const char N = 9;

void playerInput(int board[][N]);
void boardDraw(int board[][N]);
bool RowCheck(int board[][N]);
bool ColCheck(int board[][N]);
bool SqrCheck(int board[][N]);

int flag;
bool gameOver(false);

int board[N][N] = { {3, 0, 6, 5, 0, 8, 4, 0, 0},
					{5, 2, 0, 0, 0, 0, 0, 0, 0},
					{0, 8, 7, 0, 0, 0, 0, 3, 1},
					{0, 0, 3, 0, 1, 0, 0, 8, 0},
					{9, 0, 0, 8, 6, 3, 0, 0, 5},
					{0, 5, 0, 0, 9, 0, 6, 0, 0},
					{1, 3, 0, 0, 0, 0, 2, 5, 0},
					{0, 0, 0, 0, 0, 0, 0, 7, 4},
					{0, 0, 5, 2, 0, 6, 3, 0, 0} };

void boardDraw(int board[][N])
{
	system("cls");
	std::cout << "        " << "SUDOKU GAME! ENJOY" << "\n"
		<< "    1  2  3   4  5  6   7  8  9" << "\n"
		<< "   -----------------------------" << "\n";
	for (int row = 0; row < N; row++)
	{

		std::cout << row + 1 << " ";
		for (int col = 0; col < N; col++)
		{
			if (col == 3 || col == 6)
			{
				std::cout << " |";
			}
			else if (col != N)
			{
				std::cout << " ";
			}
			std::cout << " " << board[row][col];
		}
		if (row == 2 || row == 5)
		{
			std::cout << "\n" << "   ---------+---------+---------";
		}
		else if (row == 8)
		{
			std::cout << "\n" << "   -----------------------------" << "\n";
		}
		std::cout << "\n";
	}

}
//function allowing player to input his numbers in sudoku board, TODO -> assign base numbers to "protected" part so player can repair his mistakes but without making him opportunity to change base values
void playerInput(int board[][N])
{
	int numberOne;
	int numberTwo;
	int value;

	std::cout << "Please input coordinates of the square u want to fill (You can choose only those with '0') " << "\n" << "Row: ";
	std::cin >> numberOne;
	std::cout << "\n" << "And column: ";
	std::cin >> numberTwo;

	int sum = numberOne + numberTwo;


	if (board[numberOne - 1][numberTwo - 1] != 0 || numberOne < 1 || numberOne > 9 || numberTwo < 1 || numberTwo > 9)
	{
		std::cout << "Wrong coordinate! Try again";
		std::cout << "\n";
		playerInput(board);
	}
	else
	{
		std::cout << "Which value u want to at " << numberOne << " " << numberTwo << ": ";
		std::cin >> value;

		if (value < 1 || value > 9)
		{
			std::cout << "Wrong range of number! Please enter values between 1-9!" << "\n";
			std::cin >> value;
			board[numberOne - 1][numberTwo - 1] = value;
		}
		else
		{
			std::cout << "\n";
			board[numberOne - 1][numberTwo - 1] = value;
		}
	}
}
//checking input in all rows
bool RowCheck(int board[][N])
{
	for (int row = 0; row < N; row++)
	{
		flag = 0;
		for (int col = 0; col < N; col++)
		{
			if (board[row][col] < 1 || board[row][col] > 9)
			{
				return false;
			}
			if ((flag & (1 << board[row][col])) != 0)
			{
				return false;
			}
			flag |= (1 << board[row][col]);
		}
	}
	return true;
}
//same but with columns
bool ColCheck(int board[][N])
{
	for (int col = 0; col < N; col++)
	{
		flag = 0;
		for (int row = 0; row < N; row++)
		{
			if (board[row][col] < 1 || board[row][col] > 9)
			{
				return false;
			}
			if ((flag & (1 << board[row][col])) != 0)
			{
				return false;
			}
			flag |= (1 << board[row][col]);
		}
	}
	return true;
}
//checking squares 3x3
bool SqrCheck(int board[][N])
{
	for (int box = 0; box < N; box++)
	{
		flag = 0;
		for (int i = 0; i < N; i++)
		{
			int col = (box % 3) * 3;
			int row = ((int)(box / 3)) * 3;

			if ((flag & (1 << board[row][col])) != 0)
			{
				return false;
			}
			flag |= (1 << board[row][col]);
		}
	}
	return true;
}


int main()
{

	while (gameOver == false)
	{
		boardDraw(board);
		playerInput(board);
		RowCheck(board);
		ColCheck(board);
		SqrCheck(board);
		//running tests if checking of input is working properly, TODO -> delete at the end this part of code
		if (RowCheck(board) == true)
		{
			std::cout << "\n" << "ROWS ARE TRUE" << "\n";
		}
		else
		{
			std::cout << "\n" << "ROWS ARE FALSE" << "\n";
		}
		if (ColCheck(board) == true)
		{
			std::cout << "\n" << "COLS ARE TRUE" << "\n";
		}
		else
		{
			std::cout << "\n" << "COLS ARE FALSE" << "\n";
		}
		if(SqrCheck(board) == true) 
		{
			std::cout << "\n" << "SQRS ARE TRUE" << "\n";
		}
		else
		{
			std::cout << "\n" << "SQRS ARE FALSE" << "\n";
		}
		//checking if player solved sudoku, TODO -> adding option to play again
		if (RowCheck(board) == true && ColCheck(board) == true && SqrCheck(board) == true)
		{
			gameOver = true;
			std::cout << "\n" << "Congratulations! Game is over" << "\n";
		}
		else
		{
			gameOver = false;
		}
	}

	return 0;
}